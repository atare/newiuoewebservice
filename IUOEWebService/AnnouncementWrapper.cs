using System;
/*
 * Refer to LICENSE_for_samples.txt for license details relating to this file
 */

namespace IUOEWebService
{
	/// <summary>
	/// Summary description for AnnouncementWrapper.
	/// </summary>
	public class AnnouncementWrapper
	{
		public static string COURSE_TYPE = "blackboard.data.announcement.Announcement$Type:COURSE";
		public static string SYSTEM_TYPE = "blackboard.data.announcement.Announcement$Type:SYSTEM";
		private AnnouncementWrapper()
		{
		}
		private AnnouncementWS announcementWs;
		private WebserviceWrapper ws;

		public AnnouncementWrapper(WebserviceWrapper wsw, AnnouncementWS ann)
		{
			announcementWs = ann;
			ws = wsw;
		}
		public string[] saveCourseAnnouncements(string courseId, AnnouncementVO[] announcements)
		{
			try 
			{
				return announcementWs.createCourseAnnouncements(courseId,announcements);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] updateCourseAnnouncements(string courseId, AnnouncementVO[] announcements)
		{
			try 
			{
				return announcementWs.updateCourseAnnouncements(courseId,announcements);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public AnnouncementVO[] loadCourseAnnouncements(string courseId, AnnouncementAttributeFilter filter)
		{
			try 
			{
				return announcementWs.getCourseAnnouncements(courseId,filter);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] deleteCourseAnnouncements(string courseId, string[]announcementIds)
		{
			try 
			{
				return announcementWs.deleteCourseAnnouncements(courseId,announcementIds);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string[] saveOrgAnnouncements(string courseId, AnnouncementVO[] announcements)
		{
			try 
			{
				return announcementWs.createOrgAnnouncements(courseId,announcements);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] updateOrgAnnouncements(string courseId, AnnouncementVO[] announcements)
		{
			try 
			{
				return announcementWs.updateOrgAnnouncements(courseId,announcements);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public AnnouncementVO[] loadOrgAnnouncements(string courseId, AnnouncementAttributeFilter filter)
		{
			try 
			{
				return announcementWs.getOrgAnnouncements(courseId,filter);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] deleteOrgAnnouncements(string courseId, string[]announcementIds)
		{
			try 
			{
				return announcementWs.deleteOrgAnnouncements(courseId,announcementIds);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
	
		public string[] saveSystemAnnouncements(AnnouncementVO[] announcements)
		{
			try 
			{
				return announcementWs.createSystemAnnouncements(announcements);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] updateSystemAnnouncements(AnnouncementVO[] announcements)
		{
			try 
			{
				return announcementWs.updateSystemAnnouncements(announcements);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public AnnouncementVO[] loadSystemAnnouncements(AnnouncementAttributeFilter filter)
		{
			try 
			{
				return announcementWs.getSystemAnnouncements(filter);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] deleteSystemAnnouncements(string[]announcementIds)
		{
			try 
			{
				return announcementWs.deleteSystemAnnouncements(announcementIds);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
	}
}
