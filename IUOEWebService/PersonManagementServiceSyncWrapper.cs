using System;
/*
 * Refer to LICENSE_for_samples.txt for license details relating to this file
 */

namespace IUOEWebService
{
	/// <summary>
	/// Summary description for PersonManagementServiceSyncWrapper.
	/// </summary>
	public class PersonManagementServiceSyncWrapper
	{
		/* Temporarily disable IMS service references
		private PersonManagementServiceSync personManagementWs;
		private WebserviceWrapper ws;

		public PersonManagementServiceSyncWrapper(WebserviceWrapper wsw, PersonManagementServiceSync x)
		{
			personManagementWs = x;
			ws = wsw;
		}
		private String getOneStatusText(StatusInfoDType stat)
		{
			// TODO - better job of this :)
			return (stat.codeMajor +"") + ":"+(stat.codeMinor + "") + ":"+
				stat.description.text + ":"+ stat.messageIdRef + ":"+
				stat.operationIdRef + ":"+(stat.severity+"");
		}
		private String syncResponseToDebugText(syncResponseHeaderInfo resp) 
		{
			string mid = resp.messageIdentifier;
			object status = resp.Item;
			string statusText = "unknown";
			if (status != null) 
			{
				if (status.GetType() == typeof(StatusInfoDType))
				{
					StatusInfoDType stat = (StatusInfoDType)status;
					statusText = getOneStatusText(stat);
				
				} 
				else if (status.GetType() == typeof(StatusInfoDType[]))
				{
					// TODO - this block doesn't actually work
					foreach (StatusInfoDType st in (StatusInfoDType[])status) 
					{
						statusText = statusText + getOneStatusText(st) + ", ";
					}
				}
			}
			return "MessageId:" + mid + ",Status:" + statusText;
		}
		public string createPerson()
		{
			// TODO - take arguments to this method.. and return a nicer result :)
			try
			{
				createPersonRequest cpr = new createPersonRequest();
				sourcedId sourcedid = new sourcedId();
				PersonDType person = new PersonDType();
				sourcedid.identifier = "TODO:id";
				UserIdDType userId = new UserIdDType();
				userId.passWord = "TODO:password";
				userId.userIdValue = "TODO:dotnetuser";
				person.userId = userId;
				person.formatName = "TODO: dot net format name";
				cpr.person = person;
				cpr.sourcedId = sourcedid;
				personManagementWs.syncRequestHeaderInfoValue = new syncRequestHeaderInfo();
				personManagementWs.syncRequestHeaderInfoValue.messageIdentifier = "TODO:inmid";
				createPersonResponse cpresp = personManagementWs.createPerson(cpr);
				string statusText = syncResponseToDebugText(personManagementWs.syncResponseHeaderInfoValue);
			
				return statusText;
			} 
			catch (Exception e)
			{
				ws.setLastError(e);
			}
			return "Failed";
		}
	*/
	}
}
