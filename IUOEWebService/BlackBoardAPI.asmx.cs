﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Microsoft.Web.Services3;
using Microsoft.Web.Services3.Security.Tokens;
using System.IO;




namespace IUOEWebService
{


   
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BlackBoardAPI : System.Web.Services.WebService
    {
          
        //[WebMethod]
        //public string simpleMethod(String srt)
        //{
        //    return "Hello " + srt;
            
        //}

        //[WebMethod]
        //public int anotherSimpleMethod(int firstNum, int secondNum)
        //{
        //   int result = firstNum + secondNum;
        //   CourseIdVO coursevo = new CourseIdVO();
        //   CreateLog("Object Created : " + coursevo.ToString());
        //   CreateLog(firstNum.ToString() + " + " + secondNum.ToString() + " = " + result);
        //    return result ;
            
            
        //}

        public void CreateLog(string Message)
        {
            System.IO.StreamWriter file = new System.IO.StreamWriter(Server.MapPath("/") + "/" + "test.txt",true);
            file.WriteLine("{0} {1} : {2}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString(), Message);
            file.WriteLine("-----------------------------------------------------------");
            file.WriteLine("");
            file.Close();


            
           
        }


        [WebMethod]
        public CourseVO[] GetAllCourses()
        {
            bool bLoginWasSuccess = false;
            bool bLoginResultReturned = false;
            // Instatiate a new Context object
            ContextWS context = new ContextWS();
            // Create a cookie container
            context.CookieContainer = new System.Net.CookieContainer();
            // Set a token with the password initially set as nosession
            UsernameToken usernametoken = new UsernameToken("session", "nosession", PasswordOption.SendPlainText);
            // Get a response from the webservice
            initializeResponse resp = context.initialize();
            // Finalize the token with the password set by the response
            usernametoken = new UsernameToken("session", resp.@return, PasswordOption.SendPlainText);
            context.login("technomile", "technomile#1", "317510", "example_tool", "", 5000, true, out bLoginWasSuccess, out bLoginResultReturned);

            if (bLoginResultReturned)
            {
                if (bLoginWasSuccess)
                {
                    WebserviceWrapper wsw = new WebserviceWrapper("http://iuoentf.blackboard.com", "317510", "example_tool", 5000);
                    wsw.initialize_v1();

                    if (wsw.loginUser("technomile", "technomile#1"))
                    {
                        CourseWrapper courseWrap = wsw.getCourseWrapper();
                        CourseFilter courseFilter = new CourseFilter();
                        courseFilter.filterType = 0;
                        courseFilter.available = 1;
                        courseFilter.availableSpecified = true;
                        CourseVO[] courses = courseWrap.loadCourses(courseFilter);
                        return courses;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
            
           
        }

        [WebMethod]
        public CourseVO[] GetAllCoursesByCourseID(string courseid)
        {
            WebserviceWrapper wsw = new WebserviceWrapper("http://iuoentf.blackboard.com", "317510", "example_tool", 5000);
            wsw.initialize_v1();

            if (wsw.loginUser("technomile", "technomile#1"))
            {
                CourseWrapper courseWrap = wsw.getCourseWrapper();
                CourseFilter courseFilter = new CourseFilter();
                courseFilter.filterType = 0;
                CourseVO[] courses = courseWrap.loadCourses(courseFilter).Where(s => s.courseId.Equals(courseid)).ToArray();
                return courses;

            }
            else
            {
                return null;
            }

        }


        [WebMethod]
        public void  GetAllUsers()
        {
            //WebserviceWrapper wsw = new WebserviceWrapper("http://iuoentf.blackboard.com", "317510", "example_tool", 5000);
            //wsw.initialize_v1();

            //if (wsw.loginUser("technomile", "technomile#1"))
            //{
            //    UserWrapper userWrap = wsw.getUserWrapper();
                
                
            //    UserFilter userFilter = new UserFilter();
            //    userFilter.filterType = 1;
            //    userFilter.
            //    UserVO[] users = userWrap.getUser(userFilter);
                
            //    return users;

            //}
            //else
            //{
            //    return null;
            //}

        }

        public void TestMethod()
        {
            string test1 = "test1";
            test1 += "test2";
        }




    }
}