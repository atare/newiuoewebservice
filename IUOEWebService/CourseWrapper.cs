using System;
/*
 * Refer to LICENSE_for_samples.txt for license details relating to this file
 */

namespace IUOEWebService
{
	/// <summary>
	/// Summary description for CourseWrapper.
	/// </summary>
	public class CourseWrapper
	{
		private CourseWS courseWs;
		private WebserviceWrapper ws;

		public CourseWrapper(WebserviceWrapper wsw, CourseWS x)
		{
			courseWs = x;
			ws = wsw;
		}

		public string saveCourse(CourseVO course) 
		{
			try 
			{
				return courseWs.saveCourse(course);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string createCourse(CourseVO course) 
		{
			try 
			{
				return courseWs.createCourse(course);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string updateCourse(CourseVO course) 
		{
			try 
			{
				return courseWs.updateCourse(course);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string createOrg(CourseVO course) 
		{
			try 
			{
				return courseWs.createOrg(course);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string updateOrg(CourseVO course) 
		{
			try 
			{
				return courseWs.updateOrg(course);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}


		public CourseVO[] loadCourses(CourseFilter f)
		{
			try 
			{
				return courseWs.getCourse(f);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public CourseVO[] loadOrgs(CourseFilter f)
		{
			try 
			{
				return courseWs.getOrg(f);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string[] deleteCourses(string [] ids) 
		{
			try 
			{
				return courseWs.deleteCourse(ids);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string[] deleteOrgs(string [] ids) 
		{
			try 
			{
				return courseWs.deleteOrg(ids);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string saveCartridge(CartridgeVO cartridge) 
		{
			try 
			{
				return courseWs.saveCartridge(cartridge);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public CartridgeVO loadCartridge(string id)
		{
			try 
			{
				return courseWs.getCartridge(id);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		
		public string deleteCartridge(string id)
		{
			try 
			{
				return courseWs.deleteCartridge(id);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string saveCourseCategory(CategoryVO category) 
		{
			try 
			{
				return courseWs.saveCourseCategory(category);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string saveOrgCategory(CategoryVO category) 
		{
			try 
			{
				return courseWs.saveOrgCategory(category);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public CategoryVO[] loadCategories(CategoryFilter filter) 
		{
			try 
			{
				return courseWs.getCategories(filter);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] deleteCourseCategories(string [] ids) 
		{
			try 
			{
				if (ids != null && ids.Length > 0) 
				{
					return courseWs.deleteCourseCategory(ids);
				}
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] deleteOrgCategories(string [] ids) 
		{
			try 
			{
				if (ids != null && ids.Length > 0) 
				{
					return courseWs.deleteOrgCategory(ids);
				}
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string saveGroup(string courseId, GroupVO group) 
		{
			try 
			{
				return courseWs.saveGroup(courseId, group);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public GroupVO[] loadGroups(string courseId, GroupFilter filter) 
		{
			try 
			{
				return courseWs.getGroup(courseId, filter);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] deleteGroup(GroupVO[] groups) 
		{
			try 
			{
				if (groups != null && groups.Length > 0) 
				{
					string courseId = groups[0].courseId;
					string [] ids = new string[groups.Length];
					for (int i=0;i<groups.Length;i++) 
					{
						ids[i] = groups[i].id;
					}
					return courseWs.deleteGroup(courseId, ids);
				}
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string saveStaffInfo(string courseId, StaffInfoVO staff) 
		{
			try 
			{
				return courseWs.saveStaffInfo(courseId, staff);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public StaffInfoVO[] loadStaffInfo(string courseId) 
		{
			try 
			{
				return courseWs.getStaffInfo(courseId);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] deleteStaffInfo(string courseId, string [] ids) 
		{
			try 
			{
				return courseWs.deleteStaffInfo(courseId, ids);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] getAvailableGroupTools(string courseId)
		{
			try 
			{
				return courseWs.getAvailableGroupTools(courseId);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public ClassificationVO [] getClassifications(string idMask)
		{
			try 
			{
				return courseWs.getClassifications(idMask);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string saveCourseCategoryMembership(CategoryMembershipVO categoryMembership) 
		{
			try 
			{
				return courseWs.saveCourseCategoryMembership(categoryMembership);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string saveOrgCategoryMembership(CategoryMembershipVO categoryMembership) 
		{
			try 
			{
				return courseWs.saveOrgCategoryMembership(categoryMembership);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public CategoryMembershipVO [] getCourseCategoryMembership(CategoryMembershipFilter filter)
		{
			try 
			{
				return courseWs.getCourseCategoryMembership(filter);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public CategoryMembershipVO [] getOrgCategoryMembership(CategoryMembershipFilter filter)
		{
			try 
			{
				return courseWs.getOrgCategoryMembership(filter);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] deleteCourseCategoryMembership(string [] ids)
		{
			try 
			{
				return courseWs.deleteCourseCategoryMembership(ids);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] deleteOrgCategoryMembership(string [] ids)
		{
			try 
			{
				return courseWs.deleteOrgCategoryMembership(ids);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public bool changeCourseCategoryBatchUid(string categoryId, string newBatchUid)
		{
			try 
			{
				bool ret;
				bool retspec;
				courseWs.changeCourseCategoryBatchUid(categoryId,newBatchUid,out ret,out retspec);
				return ret;
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return false;
		}
		public bool changeOrgCategoryBatchUid(string categoryId, string newBatchUid)
		{
			try 
			{
				bool ret;
				bool retspec;
				courseWs.changeOrgCategoryBatchUid(categoryId,newBatchUid,out ret,out retspec);
				return ret;
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return false;
		}
		public bool changeCourseBatchUid(string oldBatchUid, string newBatchUid)
		{
			try 
			{
				bool ret;
				bool retspec;
				courseWs.changeCourseBatchUid(oldBatchUid,newBatchUid,out ret,out retspec);
				return ret;
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return false;
		}
		public bool changeCourseDataSourceId(string courseId, string newDataSourceId)
		{
			try 
			{
				bool ret;
				bool retspec;
				courseWs.changeCourseDataSourceId(courseId,newDataSourceId,out ret,out retspec);
				return ret;
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return false;
		}
        public bool setCourseBannerImage(string courseId, bool removeOnly, string fileName, System.Byte[] bytes)
        {
            try
            {
                bool ret;
                bool retspec;
                courseWs.setCourseBannerImage(courseId, removeOnly, true, fileName, bytes, out ret, out retspec);
                return ret;
            }
            catch (Exception e)
            {
                ws.setLastError(e);
            }
            return false;
        }

        public CourseVO[] loadCoursesInTerm(string termId)
        {
            try
            {
                return courseWs.loadCoursesInTerm(termId);
            }
            catch (Exception e)
            {
                ws.setLastError(e);
            }
            return null;
        }

        public bool addCourseToTerm(string courseId, string termId)
        {
            try
            {
                bool ret;
                bool retspec;
                courseWs.addCourseToTerm(courseId, termId, out ret, out retspec);
                return ret;
            }
            catch (Exception e)
            {
                ws.setLastError(e);
            }
            return false;
        }

        public bool removeCourseFromTerm(string courseId)
        {
            try
            {
                bool ret;
                bool retspec;
                courseWs.removeCourseFromTerm(courseId, out ret, out retspec);
                return ret;
            }
            catch (Exception e)
            {
                ws.setLastError(e);
            }
            return false;
        }

        public TermVO loadTerm(string termId)
        {
            try
            {
                return courseWs.loadTerm(termId);
            }
            catch (Exception e)
            {
                ws.setLastError(e);
            }
            return null;
        }

        public TermVO loadTermByCourseId(string courseId)
        {
            try
            {
                return courseWs.loadTermByCourseId(courseId);
            }
            catch (Exception e)
            {
                ws.setLastError(e);
            }
            return null;
        }

        public string saveTerm(TermVO term)
        {
            try
            {
                return courseWs.saveTerm(term);
            }
            catch (Exception e)
            {
                ws.setLastError(e);
            }
            return null;
        }

        public bool deleteTerm(string termId)
        {
            try
            {
                bool ret;
                bool retspec;
                courseWs.deleteTerm(termId, out ret, out retspec);
                return ret;
            }
            catch (Exception e)
            {
                ws.setLastError(e);
            }
            return false;
        }

        public TermVO[] loadTerms(bool onlyAvailable)
        {
            try
            {
                return courseWs.loadTerms(onlyAvailable, true);
            }
            catch (Exception e)
            {
                ws.setLastError(e);
            }
            return null;
        }

        public TermVO[] loadTermsByName(string name)
        {
            try
            {
                return courseWs.loadTermsByName(name);
            }
            catch (Exception e)
            {
                ws.setLastError(e);
            }
            return null;
        }
    }
}
