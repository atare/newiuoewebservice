using System;

/*
 * Refer to LICENSE_for_samples.txt for license details relating to this file
 */

namespace IUOEWebService
{
	/// <summary>
	/// Summary description for ContextWrapper.
	/// </summary>
	public class ContextWrapper
	{
		private ContextWS contextWs;
		private WebserviceWrapper ws;

		public ContextWrapper(WebserviceWrapper wsw, ContextWS x)
		{
			contextWs = x;
			ws = wsw;
		}

		public String getSystemInstallationId()
		{
			try 
			{
				return contextWs.getSystemInstallationId().@return;
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
				return null;
			}
		}

		public RegisterToolResultVO registerTool(string vendorId, string programId, string description, string toolRegistrationPassword, string initialSharedSecret, string [] requiredToolMethods, string [] requiredTicketMethods) 
		{
			RegisterToolResultVO res;
			try 
			{
				res = contextWs.registerTool(vendorId, programId, toolRegistrationPassword, description, initialSharedSecret, requiredToolMethods, requiredTicketMethods);
				return res;
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			res = new RegisterToolResultVO();
			res.status = false;
			res.proxyToolGuid = "";
			return res;
		}

		public CourseIdVO[] getMyMemberships() 
		{
			try 
			{
				CourseIdVO [] memberships = contextWs.getMyMemberships();
				return memberships;
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;

		}
		public CourseIdVO[] getMemberships(String otherUser) 
		{
			try 
			{
				CourseIdVO [] memberships = contextWs.getMemberships(otherUser);
				return memberships;
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public bool extendSessionLife(long seconds)
		{
			bool ret = false;
			try 
			{
				bool retspec;
				contextWs.extendSessionLife(seconds, true, out ret, out retspec);
				return ret;
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return ret;
		}

		public bool emulateUser(String otherUser) 
		{
			bool ret = false;
			try 
			{
				bool retspec;
				contextWs.emulateUser(otherUser, out ret, out retspec);
				return ret;
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return ret;
		}

		public bool logout() 
		{
			bool res = false;
			try 
			{
				res = contextWs.logout().@return;
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return res;
		}

	}
}
