using System;

/*
 * Refer to LICENSE_for_samples.txt for license details relating to this file
 */

namespace IUOEWebService
{
	/// <summary>
	/// Summary description for CalendarWrapper.
	/// </summary>
	public class CalendarWrapper
	{
		public static string PERSONAL_TYPE = "PERSONAL";
		public static string COURSE_TYPE = "COURSE";
		public static string INSTITUTION_TYPE = "INSTITUTION";
		private CalendarWrapper()
		{
		}
		private CalendarWS calendarWs;
		private WebserviceWrapper ws;

		public CalendarWrapper(WebserviceWrapper wsw, CalendarWS cal)//called from WebServiceWrapper.cs
		{
			calendarWs = cal;
			ws = wsw;
		}

		public string[] savePersonalCalendarItem(CalendarItemVO[] items)
		{
			try 
			{
				return calendarWs.savePersonalCalendarItem(items);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] createPersonalCalendarItem(CalendarItemVO[] items)
		{
			try 
			{
				return calendarWs.createPersonalCalendarItem(items);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] canUpdatePersonalCalendarItem(String[] items)
		{
			try 
			{
				return calendarWs.canUpdatePersonalCalendarItem(items);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] updatePersonalCalendarItem(CalendarItemVO[] items)
		{
			try 
			{
				return calendarWs.updatePersonalCalendarItem(items);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string [] deletePersonalCalendarItem(string[]itemsIds)
		{
			try 
			{
				return calendarWs.deletePersonalCalendarItem(itemsIds);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] saveCourseCalendarItem(string courseId,CalendarItemVO[] items)
		{
			try 
			{
				return calendarWs.saveCourseCalendarItem(courseId,items);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] createCourseCalendarItem(string courseId,CalendarItemVO[] items)
		{
			try 
			{
				return calendarWs.createCourseCalendarItem(courseId,items);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string [] canUpdateCourseCalendarItem(string courseId,String[] items)
		{
			try 
			{
				return calendarWs.canUpdateCourseCalendarItem(courseId,items);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string [] updateCourseCalendarItem(string courseId,CalendarItemVO[] items)
		{
			try 
			{
				return calendarWs.updateCourseCalendarItem(courseId,items);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

	
		public string [] deleteCourseCalendarItem(string courseId,string[]itemsIds)
		{
			try 
			{
				return calendarWs.deleteCourseCalendarItem(courseId,itemsIds);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string[] saveInstitutionCalendarItem(CalendarItemVO[] items)
		{
			try 
			{
				return calendarWs.createInstitutionCalendarItem(items);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string[] createInstitutionCalendarItem(CalendarItemVO[] items)
		{
			try 
			{
				return calendarWs.createInstitutionCalendarItem(items);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		
		public string[]  canUpdateInstitutionCalendarItem(String[] items)
		{
			try 
			{
				return calendarWs.canUpdateInstitutionCalendarItem(items);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] updateInstitutionCalendarItem(CalendarItemVO[] items)
		{
			try 
			{
				return calendarWs.updateInstitutionCalendarItem(items);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

	
		public string[] deleteInstitutionCalendarItem(string[]itemsIds)
		{
			try 
			{
				return calendarWs.deleteInstitutionCalendarItem(itemsIds);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

	
		

		public CalendarItemVO[] getCalendarItem(CalendarItemFilter filter)
		{
			try 
			{
				return calendarWs.getCalendarItem(filter);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}




	}
}
