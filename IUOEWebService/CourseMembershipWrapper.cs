using System;
/*
 * Refer to LICENSE_for_samples.txt for license details relating to this file
 */

namespace IUOEWebService
{
	/// <summary>
	/// Summary description for CourseMembershipWrapper.
	/// </summary>
	public class CourseMembershipWrapper
	{
		private CourseMembershipWS courseMembershipWs;
		private WebserviceWrapper ws;

		public CourseMembershipWrapper(WebserviceWrapper wsw, CourseMembershipWS x)
		{
			courseMembershipWs = x;
			ws = wsw;
		}

		public CourseMembershipRoleVO[] loadRoles(string [] f)
		{
			try 
			{
				return courseMembershipWs.getCourseRoles(f);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] saveCourseMembership(string courseid, CourseMembershipVO []memberships)
		{
			try 
			{
				return courseMembershipWs.saveCourseMembership(courseid,memberships);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string[] saveGroupMembership(string courseid, GroupMembershipVO []memberships)
		{
			try 
			{
				return courseMembershipWs.saveGroupMembership(courseid,memberships);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public CourseMembershipVO[] loadCourseMembership(string courseid, MembershipFilter f)
		{
			try 
			{
				return courseMembershipWs.getCourseMembership(courseid, f);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public GroupMembershipVO[] loadGroupMembership(string courseid, MembershipFilter f)
		{
			try 
			{
				return courseMembershipWs.getGroupMembership(courseid, f);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] deleteCourseMembership(string courseid, string [] membershipIds)
		{
			try 
			{
				return courseMembershipWs.deleteCourseMembership(courseid, membershipIds);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] deleteGroupMembership(string courseid, string [] membershipIds)
		{
			try 
			{
				return courseMembershipWs.deleteGroupMembership(courseid, membershipIds);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
	}
}
