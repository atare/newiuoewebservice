using System;
/*
 * Refer to LICENSE_for_samples.txt for license details relating to this file
 */

namespace IUOEWebService
{
	/// <summary>
	/// Summary description for UserWrapper.
	/// </summary>
	public class UserWrapper
	{
		private UserWS userWs;
		private WebserviceWrapper ws;

		public UserWrapper(WebserviceWrapper wsw, UserWS x)
		{
			userWs = x;
			ws = wsw;
		}
		public string[] createUsers(UserVO[] users) 
		{
			try 
			{
				return userWs.saveUser(users);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] deleteUsers(string [] ids) 
		{
			try 
			{
				return userWs.deleteUser(ids);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string [] deleteUserByInstitutionRole(string [] roles) 
		{
			try 
			{
				return userWs.deleteUserByInstitutionRole(roles);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public UserVO[] getUser(UserFilter f) 
		{
			try 
			{
				return userWs.getUser(f);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string[] createAddressBookEntries(AddressBookEntryVO[] addresses) 
		{
			try 
			{
				return userWs.saveAddressBookEntry(addresses);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public AddressBookEntryVO[] getAddressBookEntry(UserFilter f)
		{
			try 
			{
				return userWs.getAddressBookEntry(f);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] deleteAddressBookEntries(string [] ids) 
		{
			try 
			{
				return userWs.deleteAddressBookEntry(ids);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] saveObserverAssociation(ObserverAssociationVO [] associations) 
		{
			try 
			{
				return userWs.saveObserverAssociation(associations);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public ObserverAssociationVO[] getObservee(String[] ids) 
		{
			try 
			{
				return userWs.getObservee(ids);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] deleteObserverAssociation(ObserverAssociationVO[] assoc) 
		{
			try 
			{
				return userWs.deleteObserverAssociation(assoc);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public bool changeUserBatchUid(string originalBatchUid, string newBatchUid) 
		{
			try 
			{
				bool ret;
				bool retspec;
				userWs.changeUserBatchUid(originalBatchUid, newBatchUid, out ret, out retspec);
				return ret;
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return false;
		}
		public bool changeUserDataSourceId(string userId, string newDataSourceId) 
		{
			try 
			{
				bool ret;
				bool retspec;
				userWs.changeUserDataSourceId(userId, newDataSourceId, out ret, out retspec);
				return ret;
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return false;
		}
		public PortalRoleVO [] getInstitutionRoles() 
		{
			try 
			{
				return userWs.getInstitutionRoles(null);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] getSystemRoles() 
		{
			try 
			{
				return userWs.getSystemRoles(null);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public UserRoleVO [] getUserInstitutionRoles(String []ids) 
		{
			try 
			{
				return userWs.getUserInstitutionRoles(ids);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
	}
}
