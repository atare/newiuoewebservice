using System;
/*
 * Refer to LICENSE_for_samples.txt for license details relating to this file
 */

namespace IUOEWebService
{
	/// <summary>
	/// Summary description for GradebookWrapper.
	/// </summary>
	public class GradebookWrapper
	{
		private GradebookWS gradebookWs;
		private WebserviceWrapper ws;

		public GradebookWrapper(WebserviceWrapper wsw, GradebookWS x)
		{
			gradebookWs = x;
			ws = wsw;

		}

		public ColumnVO[] getColumns(string courseid, ColumnFilter filter) 
		{
			try 
			{
				return gradebookWs.getGradebookColumns(courseid, filter);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public string[] saveColumns(string courseid, ColumnVO[] columns) 
		{
			try 
			{
				return gradebookWs.saveColumns(courseid, columns);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] updateColumnAttribute(string courseId, ColumnVO[] columns, int attribute) 
		{
			try 
			{
				return gradebookWs.updateColumnAttribute(courseId, columns, attribute, true);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		
		public string[] saveAttempts(string courseId, AttemptVO[] attempts)
		{
			try 
			{
				return gradebookWs.saveAttempts(courseId, attempts);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] saveGradebookTypes(string courseId, GradebookTypeVO[] gradebookTypes) 
		{
			try 
			{
				return gradebookWs.saveGradebookTypes(courseId, gradebookTypes);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] saveGrades(string courseId, ScoreVO[] grades, bool overrideIfManual) 
		{
			try 
			{
				return gradebookWs.saveGrades(courseId, grades, overrideIfManual, true);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] saveGradingSchemas(string courseId, GradingSchemaVO[] schemas) 
		{
			try 
			{
				return gradebookWs.saveGradingSchemas(courseId, schemas);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] deleteColumn(string courseId, string [] columnIds, bool onlyIfEmpty) 
		{
			try 
			{
				return gradebookWs.deleteColumns(courseId,columnIds,onlyIfEmpty,true);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public AttemptVO[] getAttempts(string courseId, AttemptFilter filter) 
		{
			try 
			{
				return gradebookWs.getAttempts(courseId, filter);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public ScoreVO[] getGrades(string courseId, ScoreFilter filter) 
		{
			try 
			{
				return gradebookWs.getGrades(courseId, filter);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] deleteGrades(string courseId, string[]ids) 
		{
			try 
			{
				return gradebookWs.deleteGrades(courseId, ids); 
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] deleteAttempts(string courseId, string[]ids) 
		{
			try 
			{
				return gradebookWs.deleteAttempts(courseId, ids); 
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] deleteGradebookTypes(string courseId, string[]ids) 
		{
			try 
			{
				return gradebookWs.deleteGradebookTypes(courseId, ids); 
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] deleteGradingSchemas(string courseId, string[]ids) 
		{
			try 
			{
				return gradebookWs.deleteGradingSchemas(courseId, ids); 
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public GradebookTypeVO[] getGradebookTypes(string courseId, GradebookTypeFilter filter) 
		{
			try 
			{
				return gradebookWs.getGradebookTypes(courseId, filter);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public GradingSchemaVO[] getGradingSchemas(string courseId, GradingSchemaFilter filter) 
		{
			try 
			{
				return gradebookWs.getGradingSchemas(courseId, filter);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
	}
}
