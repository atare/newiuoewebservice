using System;
/*
 * Refer to LICENSE_for_samples.txt for license details relating to this file
 */

namespace IUOEWebService
{
	/// <summary>
	/// Summary description for ContentWrapper.
	/// </summary>
	public class ContentWrapper
	{
		private ContentWS contentWs;
		private WebserviceWrapper ws;

		public ContentWrapper(WebserviceWrapper wsw, ContentWS x)
		{
			contentWs = x;
			ws = wsw;
		}

		public string saveContent(string courseid, ContentVO content) 
		{
			try 
			{
				CourseIdVO courseId = new CourseIdVO();
				courseId.externalId = courseid;
				return contentWs.saveContent(courseId, content);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public ContentVO[] loadFilteredContent(string courseId, ContentFilter filter) 
		{
			try 
			{
				return contentWs.getFilteredContent(courseId, filter);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string[] deleteContents(string courseId, string[] ids) 
		{
			try 
			{
				return contentWs.deleteContents(courseId, ids);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public ContentFileMetadataVO [] getContentFiles(string courseId, string contentid)
		{
			try 
			{
				return contentWs.getContentFiles(courseId, contentid);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] deleteContentFiles(string courseId, string [] ids) 
		{
			try 
			{
				return contentWs.deleteContentFiles(courseId, ids);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string saveCourseTOC(string courseId, CourseTOCVO toc, LinkVO link) 
		{
			try 
			{
				return contentWs.saveCourseTOC(courseId, toc, link);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public CourseTOCVO[] getTOCsByCourseId(string courseId) 
		{
			try 
			{
				return contentWs.getTOCsByCourseId(courseId);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] deleteCourseTocs(string courseId, string[] ids) 
		{
			try 
			{
				return contentWs.deleteCourseTOCs(courseId, ids);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public ContentStatusVO[] getCourseStatus(string courseId, ContentStatusFilter filter) 
		{
			try 
			{
				return contentWs.getFilteredCourseStatus(courseId, filter);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public LinkVO[] getLinksFrom(string courseId, string referrerType) 
		{
			try 
			{
				return contentWs.getLinksByReferrerType(courseId, referrerType);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public LinkVO[] getLinksTo(string courseId, string referrerToType) 
		{
			try 
			{
				return contentWs.getLinksByReferredToType(courseId, referrerToType);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string [] deleteLinks(string courseId, string [] linkIds)
		{
			try 
			{
				return contentWs.deleteLinks(courseId, linkIds);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string saveLink(string courseId, LinkVO link) 
		{
			try 
			{
				return contentWs.saveLink(courseId, link);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string saveContentsReviewed(string courseId, ContentsReviewedVO reviewed) 
		{
			try 
			{
				return contentWs.saveContentsReviewed(courseId, reviewed);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string addContentFile(string courseId, string contentid, ContentFileMetadataVO metadata, System.Byte[] bytes) 
		{
			try 
			{
				return contentWs.addContentFile(courseId, contentid, metadata, bytes);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		public string updateContentFileLinkName(string courseId, string contentId, string contentFileId, string newLinkName)
		{
			try 
			{
				return contentWs.updateContentFileLinkName(courseId, contentId, contentFileId, newLinkName);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public AggregateReviewStatusVO[] getReviewStatusByCourseId(string courseId) 
		{
			try 
			{
				return contentWs.getReviewStatusByCourseId(courseId);
			}			
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}
		
	}
}
