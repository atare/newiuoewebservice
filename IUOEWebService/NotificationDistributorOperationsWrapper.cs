using System;

namespace IUOEWebService
{
	/// <summary>
	/// Summary description for NotificationDistributorOperationsWrapper.
	/// </summary>
	public class NotificationDistributorOperationsWrapper
	{
		private NotificationDistributorOperationsWS notificationDistributorOperationsWs;
		private WebserviceWrapper ws;

		public NotificationDistributorOperationsWrapper(WebserviceWrapper wsw, NotificationDistributorOperationsWS x)
		{
			notificationDistributorOperationsWs = x;
			ws = wsw;
		}
	}
}
