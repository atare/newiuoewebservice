/* WebserviceWrapper.cs sample wrapper 
 * Refer to LICENSE_for_samples.txt for license details relating to this file
 */
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using Microsoft.Web.Services3;
using Microsoft.Web.Services3.Security;
using Microsoft.Web.Services3.Security.Tokens;

namespace IUOEWebService
{
	public class WebserviceWrapper 
	{
       
		private String baseUrl;

		private AnnouncementWS announcement;
		private bool announcementInitialized = false;
		private CalendarWS calendar;
		private bool calendarInitialized = false;
		private ContentWS content;
		private bool contentInitialized = false;
		private ContextWS context;
		private CourseWS course;
		private bool courseInitialized = false;
		private CourseMembershipWS courseMembership;
		private bool courseMembershipInitialized = false;
		private GradebookWS gradebook;
		private bool gradebookInitialized = false;
		private NotificationDistributorOperationsWS notificationDistributorOperationsWs;
		private bool notificationDistributorOperationsInitialized = false;

		/* Temporarily remove IMS service references
		private GroupManagementServiceSync groupManagement;
		private MembershipManagementServiceSync membershipManagement;
		private PersonManagementServiceSync personManagement;
		*/
		private UserWS user;
		private bool userInitialized = false;
		private UtilWS util;
		private bool utilInitialized = false;

		private Exception lastError;
		private String vendorId;
		private String programId;
		private long expectedLife;
		private String sessionId;

		public WebserviceWrapper(String host, String vendor, String program, long expected_life) 
		{

			vendorId = vendor;
			programId = program;
			baseUrl = host + "/webapps/ws/services";
			// Setup the basic security token indicating 'nosession'
			UsernameToken userToken = new UsernameToken("session","nosession",PasswordOption.SendPlainText);
			expectedLife = expected_life;

			initAnnouncement(userToken);
			initCalendar(userToken);
			initContent(userToken);
			initContext(userToken);
			initCourse(userToken);
			initCourseMembership(userToken);
			initGradebook(userToken);
			initNotificationDistributorOperations(userToken);
			/* Temporarily disable references to IMS web services
			initGroupManagement(userToken);
			initMembershipManagement(userToken);
			initPersonManagement(userToken);
			*/
			initUser(userToken);
			initUtil(userToken);
		}

		private void initAnnouncement(UsernameToken userToken) 
		{
			announcement = new AnnouncementWS();
			announcement.Url = baseUrl + "/Announcement.WS";
			announcement.RequestSoapContext.Security.Tokens.Add(userToken);
		}

		private void initCalendar(UsernameToken userToken) 
		{
			calendar = new CalendarWS();
			calendar.Url = baseUrl + "/Calendar.WS";
			calendar.RequestSoapContext.Security.Tokens.Add(userToken);
		}

		private void initContext(UsernameToken userToken) 
		{
			context = new ContextWS();
			context.Url = baseUrl + "/Context.WS";
			context.RequestSoapContext.Security.Tokens.Add(userToken);
		}

		private void initUtil(UsernameToken userToken) 
		{
			util = new UtilWS();
			util.Url = baseUrl + "/Util.WS";
			util.RequestSoapContext.Security.Tokens.Add(userToken);
		}

		private void initContent(UsernameToken userToken) 
		{
			content = new ContentWS();
			content.Url = baseUrl + "/Content.WS";
			content.RequestSoapContext.Security.Tokens.Add(userToken);
		}

		private void initCourse(UsernameToken userToken) 
		{
			course = new CourseWS();
			course.Url = baseUrl + "/Course.WS";
			course.RequestSoapContext.Security.Tokens.Add(userToken);
		}

		private void initCourseMembership(UsernameToken userToken) 
		{
			courseMembership = new CourseMembershipWS();
			courseMembership.Url = baseUrl + "/CourseMembership.WS";
			courseMembership.RequestSoapContext.Security.Tokens.Add(userToken);
		}

		private void initGradebook(UsernameToken userToken) 
		{
			gradebook = new GradebookWS();
			gradebook.Url = baseUrl + "/Gradebook.WS";
			gradebook.RequestSoapContext.Security.Tokens.Add(userToken);
		}

		private void initNotificationDistributorOperations(UsernameToken userToken) 
		{
			notificationDistributorOperationsWs = new NotificationDistributorOperationsWS();
			notificationDistributorOperationsWs.Url = baseUrl + "/NotificationDistributorOperations.WS";
			notificationDistributorOperationsWs.RequestSoapContext.Security.Tokens.Add(userToken);
		}

		
		/* Temporarily disable references to IMS web services

		private void initPersonManagement(UsernameToken userToken) 
		{
			personManagement = new PersonManagementServiceSync();
			personManagement.Url = baseUrl + "/PersonManagementServiceSync";
			personManagement.RequestSoapContext.Security.Tokens.Add(userToken);
		}

		private void initGroupManagement(UsernameToken userToken) 
		{
			groupManagement = new GroupManagementServiceSync();
			groupManagement.Url = baseUrl + "/GroupManagementServiceSync";
			groupManagement.RequestSoapContext.Security.Tokens.Add(userToken);
		}
		private void initMembershipManagement(UsernameToken userToken) 
		{
			membershipManagement = new MembershipManagementServiceSync();
			membershipManagement.Url = baseUrl + "/MembershipoManagementServiceSync";
			membershipManagement.RequestSoapContext.Security.Tokens.Add(userToken);
		}
		*/

		private void initUser(UsernameToken userToken) 
		{
			user = new UserWS();
			user.Url = baseUrl + "/User.WS";
			user.RequestSoapContext.Security.Tokens.Add(userToken);
		}

		public void initialize_v1() // Must be called before anything else except getServerVersions
		{
			lastError = null;
			try 
			{
				sessionId = context.initialize().@return;
				// Now that we have a session, replace the security token with that session
				UsernameToken userToken = new UsernameToken("session",sessionId,PasswordOption.SendPlainText);
				announcement.RequestSoapContext.Security.Tokens.Clear();
				announcement.RequestSoapContext.Security.Tokens.Add(userToken);
				calendar.RequestSoapContext.Security.Tokens.Clear();
				calendar.RequestSoapContext.Security.Tokens.Add(userToken);
				content.RequestSoapContext.Security.Tokens.Clear();
				content.RequestSoapContext.Security.Tokens.Add(userToken);
				context.RequestSoapContext.Security.Tokens.Clear();
				context.RequestSoapContext.Security.Tokens.Add(userToken);
				course.RequestSoapContext.Security.Tokens.Clear();
				course.RequestSoapContext.Security.Tokens.Add(userToken);
				courseMembership.RequestSoapContext.Security.Tokens.Clear();
				courseMembership.RequestSoapContext.Security.Tokens.Add(userToken);
				gradebook.RequestSoapContext.Security.Tokens.Clear();
				gradebook.RequestSoapContext.Security.Tokens.Add(userToken);
				notificationDistributorOperationsWs.RequestSoapContext.Security.Tokens.Clear();
				notificationDistributorOperationsWs.RequestSoapContext.Security.Tokens.Add(userToken);
				/* Temporarily disable references to IMS web services
				groupManagement.RequestSoapContext.Security.Tokens.Clear();
				groupManagement.RequestSoapContext.Security.Tokens.Add(userToken);
				membershipManagement.RequestSoapContext.Security.Tokens.Clear();
				membershipManagement.RequestSoapContext.Security.Tokens.Add(userToken);
				personManagement.RequestSoapContext.Security.Tokens.Clear();
				personManagement.RequestSoapContext.Security.Tokens.Add(userToken);
				*/
				user.RequestSoapContext.Security.Tokens.Clear();
				user.RequestSoapContext.Security.Tokens.Add(userToken);
				util.RequestSoapContext.Security.Tokens.Clear();
				util.RequestSoapContext.Security.Tokens.Add(userToken);
			} 
			catch (Exception e) 
			{
				lastError = e;
			}
		}

		public String getLastError() 
		{
			if (lastError == null)
				return null;

			String msg = lastError.Message;
			lastError = null;
			return msg;
		}
		public void setLastError(Exception e)
		{
			lastError = e;
		}

		private string getVersionDisplay(VersionVO v, string service) 
		{
			return service + ":" + v.version + "\r\n";
		}

		public String getServerVersions()
		{
			try 
			{
				return getVersionDisplay(context.getServerVersion(null), "ContextWS") +
					getVersionDisplay(calendar.getServerVersion(null), "CalendarWS") +
					getVersionDisplay(util.getServerVersion(null), "UtilWS") +
					getVersionDisplay( announcement.getServerVersion(null), "AnnouncementWS") +
					getVersionDisplay(content.getServerVersion(null), "ContentWS") +
					getVersionDisplay(course.getServerVersion(null), "CourseWS") +
					getVersionDisplay(courseMembership.getServerVersion(null), "CourseMembershipWS") +
					getVersionDisplay(gradebook.getServerVersion(null), "GradebookWS") +
					getVersionDisplay(notificationDistributorOperationsWs.getServerVersion(null), "notificationDistributorOperationsWS") +
					getVersionDisplay(user.getServerVersion(null), "UserWS");
			}
			catch (Exception e) 
			{
				lastError = e;
				return null;
			}
		}

		public bool loginUser(String userId, String userPassword) 
		{
			try 
			{
				bool retval = false;
				bool retspec;
				context.login(userId, userPassword, vendorId, programId, null, expectedLife, true, out retval, out retspec);
				return retval;
			} 
			catch (Exception e) 
			{
				lastError = e;
				return false;
			}
		}
		public bool loginTool(String toolPassword) 
		{
			bool ret = false;
			try 
			{
				bool retspec;
				context.loginTool(toolPassword, vendorId, programId,null, expectedLife, true, out ret, out retspec);
				return ret;
			} 
			catch (Exception e) 
			{
				lastError = e;
				return ret;
			}
		}

		public string [] getRequiredEntitlements(string serviceType, string methodName) 
		{
			if (serviceType.Equals("Context.WS"))
				return context.getRequiredEntitlements(methodName);
			else if (serviceType.Equals("Util.WS"))
				return util.getRequiredEntitlements(methodName);
			else if (serviceType.Equals("Gradebook.WS"))
				return gradebook.getRequiredEntitlements(methodName);
			else if (serviceType.Equals("Content.WS"))
				return content.getRequiredEntitlements(methodName);
			return null;
		}
	
		public String debugInfo() 
		{
			if (sessionId == null) 
			{
				return "NO session";
			} 
			else 
			{
				return sessionId;
			}
		}

		public RegisterToolResultVO registerTool(string description, string toolRegistrationPassword, string initialSharedSecret, string [] requiredToolMethods, string [] requiredTicketMethods) 
		{
			return getContextWrapper().registerTool(vendorId, programId, description,toolRegistrationPassword, initialSharedSecret, requiredToolMethods, requiredTicketMethods);
		}

		public bool logout() 
		{
			return getContextWrapper().logout();
		}

		public AnnouncementWrapper getAnnouncementWrapper() 
		{
			if (!announcementInitialized) 
			{
				bool ret = false;
				bool retspec;
				announcement.initializeAnnouncementWS(false,true,out ret, out retspec);
				announcementInitialized = true;
			}
			return new AnnouncementWrapper(this,announcement);
		}

		public CalendarWrapper getCalendarWrapper() 
		{
			if (!calendarInitialized) 
			{
				bool ret = false;
				bool retspec;
				calendar.initializeCalendarWS(false,true,out ret, out retspec);
				calendarInitialized = true;
			}
			return new CalendarWrapper(this,calendar);
		}
		public ContentWrapper getContentWrapper() 
		{
			if (!contentInitialized) 
			{
				bool ret = false;
				bool retspec;
				content.initializeVersion2ContentWS(false,true,out ret, out retspec);
				contentInitialized = true;
			}
			return new ContentWrapper(this,content);
		}
		public ContextWrapper getContextWrapper() 
		{
			return new ContextWrapper(this,context);
		}
		public CourseMembershipWrapper getCourseMembershipWrapper() 
		{
			if (!courseMembershipInitialized) 
			{
				bool ret = false;
				bool retspec;
				courseMembership.initializeCourseMembershipWS(false,true,out ret, out retspec);
				courseMembershipInitialized = true;
			}
			return new CourseMembershipWrapper(this,courseMembership);
		}
		public CourseWrapper getCourseWrapper() 
		{
			if (!courseInitialized) 
			{
				bool ret = false;
				bool retspec;
				course.initializeCourseWS(false,true,out ret, out retspec);
				courseInitialized = true;
			}
			return new CourseWrapper(this,course);
		}
		public GradebookWrapper getGradebookWrapper() 
		{
			if (!gradebookInitialized) 
			{
				bool ret = false;
				bool retspec;
				gradebook.initializeGradebookWS(false,true,out ret, out retspec);
				gradebookInitialized = true;
			}
			return new GradebookWrapper(this,gradebook);
		}
		public NotificationDistributorOperationsWrapper getNotificationDistributorOperationsWrapper() 
		{
			if (!notificationDistributorOperationsInitialized) 
			{
				bool ret = false;
				bool retspec;
				notificationDistributorOperationsWs.initializeNotificationDistributorOperationsWS(false,true,out ret, out retspec);
				notificationDistributorOperationsInitialized = true;
			}
			return new NotificationDistributorOperationsWrapper(this,notificationDistributorOperationsWs);
		}
		
		/* Temporarily disable references to IMS web services
		public wsagent.wrappers.PersonManagementServiceSyncWrapper getPersonManagementServiceSyncWrapper() 
		{
			return new wsagent.wrappers.PersonManagementServiceSyncWrapper(this,personManagement);
		}
		*/
		public UserWrapper getUserWrapper() 
		{
			if (!userInitialized) 
			{
				bool ret = false;
				bool retspec;
				user.initializeUserWS(false,true,out ret, out retspec);
				userInitialized = true;
			}
			return new UserWrapper(this,user);
		}
		public UtilWrapper getUtilWrapper() 
		{
			if (!utilInitialized) 
			{
				bool ret = false;
				bool retspec;
				util.initializeUtilWS(false,true,out ret, out retspec);
				utilInitialized = true;
			}
			return new UtilWrapper(this,util);
		}
	}
}