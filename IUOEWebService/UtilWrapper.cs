using System;
/*
 * Refer to LICENSE_for_samples.txt for license details relating to this file
 */

namespace IUOEWebService
{
	/// <summary>
	/// Summary description for UtilWrapper.
	/// </summary>
	public class UtilWrapper
	{
		private UtilWS utilWs;
		private WebserviceWrapper ws;

		public UtilWrapper(WebserviceWrapper wsw, UtilWS x)
		{
			utilWs = x;
			ws = wsw;
		}

		public bool checkEntitlement(String entitlement, CourseIdVO course) 
		{
			bool res = false;
			try 
			{
				bool resSpecified;
				utilWs.checkEntitlement(course, entitlement, out res, out resSpecified);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return res;
		}
		public bool saveSetting(int scope, String id, String key, String value) 
		{
			try 
			{
				bool ret = false;
				bool retspec;
				utilWs.saveSetting(scope,true,id,key,value,out ret, out retspec);
				return ret;
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return false;
		}

		public String loadSetting(int scope, String id, String key) 
		{
			try 
			{
				return utilWs.loadSetting(scope, true, id, key);
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

		public bool deleteSetting(int scope, String id, String key) 
		{
			try 
			{
				bool ret = false;
				bool retspec;
				utilWs.deleteSetting(scope,true, id, key, out ret, out retspec);
				return ret;
			} 
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return false;
		}

		public DataSourceVO [] getDataSources() 
		{
			try 
			{
				return utilWs.getDataSources(null);
			}
			catch (Exception e) 
			{
				ws.setLastError(e);
			}
			return null;
		}

	}
}
